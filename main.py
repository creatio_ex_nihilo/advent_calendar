import datetime
import sys


class AdventCalendar:
    min_day = 1
    max_day = 24
    month = 12
    year = datetime.datetime.now().year
    assignees = list()
    assignment = dict()

    def __init__(self, a):
        # all days of the advent calendar % assignee count must be 0
        # because everybody has to be assigned n times
        if ((self.max_day - self.min_day) + 1) % len(a) != 0:
            raise ValueError("day range must be a multiple of assignee count")
        self.assignees = a
        self.create_assignment()

    def get_assignment(self):
        return self.assignment

    def date_in_range(self, dt):
        if dt.month == self.month and self.min_day <= dt.day <= self.max_day:
            return True
        return False

    def create_assignment(self):
        for i in range(self.min_day, self.max_day + 1):
            dt = datetime.datetime(self.year, self.month, i)
            self.assignment[dt.day] = self.assignees[self.calc_assignment(dt)]

    def calc_assignment(self, dt):
        if not self.date_in_range(dt):
            raise ValueError("datetime not in range")
        # day % assignee_cnt = index assignee
        return dt.day % len(self.assignees)

    def check_day(self, dt):
        return '{:02}'.format(dt.day) + " is " + self.assignees[self.calc_assignment(dt)] + "'s day"

    def current_day(self):
        return self.check_day(datetime.datetime.now())

    def print_assignment(self):
        # calc lines
        for j in range(int(len(self.assignment) / len(self.assignees))):
            line = ""
            line_2 = ""
            # calc rows
            for i in range(len(self.assignees)):
                c = self.min_day + (j * len(self.assignees) + i)
                line += str('{:02}'.format(c) + '\t')
                line_2 += self.assignment[c] + '\t'
            print((len(line) + 3) * '-')
            print(line)
            print(line_2)


def default_shortcut():
    try:
        print(ac.current_day())
    except ValueError:
        print("it's not that time of the year, mate!")


# start
add_args = sys.argv[1:]

if len(add_args) == 0:
    ac = AdventCalendar(["S", "D", "E"])
    default_shortcut()
elif len(add_args) == 1:
    # list of assignees
    input_list = add_args[0]
    ac = AdventCalendar(input_list.split(","))
    default_shortcut()

if len(add_args) > 1:
    # list of assignees
    input_list = add_args[0]
    ac = AdventCalendar(input_list.split(","))
    # day
    input_day = int(add_args[1])
    # 0 is not a possible date, so we will use it as a placeholder for today
    if input_day == 0:
        default_shortcut()
    else:
        try:
            print(ac.check_day(input_day))
        except ValueError:
            print("it's not that time of the year, mate!")

if len(add_args) > 2:
    # print
    input_print = add_args[2]
    if input_print == "print":
        ac.print_assignment()
